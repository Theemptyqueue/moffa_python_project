John David Moffa

README

This is my Python project README file

My project will consist of the following

1. A project file written in Python

	This project consists of the following files:
	Flight_demo.py
	
	Flight_demo.py is not dependent on any text files or any other python files, 
	do not worry about accidentally deleting the input or output files by accident 
	since they are re-written after each run to simulate a days worth of flights.
	
2. An iteration file to detail what is happening at what stages of the project.
	A file that is updated with the version current build version number
	identifying what was changed as the project moved forwards.

3. A SDD for my project will consist of:
	
	a. Consist of what I plan to do 
	for this project for iteration 1 and iteration 2.
	
	b. Give an idea of how the code works
	
	c. provide an outline to follow before the actual coding begins

4. This README file.